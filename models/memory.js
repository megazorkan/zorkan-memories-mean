'use strict';

var mongoose = require('mongoose');

var memorySchema = mongoose.Schema({
	userId: String,
	cityId: String,
	startDate: Date,
	endDate: Date,
	review: String,
	rating: {
		type: Number,
		min: 1,
		max: 5
	}
});

var Memory = mongoose.model('Memory', memorySchema);
module.exports = Memory;