'use strict';

var mongoose = require('mongoose');

var countrySchema = mongoose.Schema({
	name: String,
	longitude: Number,
	latitude: Number
});

var Country = mongoose.model('Country', countrySchema);
module.exports = Country;