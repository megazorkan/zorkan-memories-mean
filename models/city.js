'use strict';

var mongoose = require('mongoose');

var citySchema = mongoose.Schema({
	name: String,
	longitude: Number,
	latitude: Number,
	countryId: String,
	numberOfVisits: {
		type: Number,
		default: 0
	}
});

var City = mongoose.model('City', citySchema);
module.exports = City;