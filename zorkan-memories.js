'use strict';

var express = require('express');
var app = express();

var connect = require('connect');

//set static middleware
app.use(express.static(__dirname + '/public'));

var cookieParser = require('cookie-parser');
app.use(cookieParser(
	'cookie parser secret key'
));

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

var session = require('express-session');
app.use(session({
	secret: 'session secret key',
	saveUninitialized: true,
	resave: false
}));

var authentication = require('./config/authentication.js')(app);

var database = require('./config/database.js')();

var routes = require('./routes.js')(app);

app.get('/*', function(req, res) {
	res.sendFile('index.html', {
		root: './public'
	});
});

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), function() {
	console.log('Express started on http://localhost:' + app.get('port') + '; Ctrl+C to terminate.');
});