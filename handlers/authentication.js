'use strict';

var passport = require('passport');
var _ = require('underscore');
var User = require('./../models/user.js');
var AccessToken = require('./../models/accessToken.js');

exports.login = function(req, res) {
	passport.authenticate('local', {
		session: false
	}, function(err, user) {

		//popravi ovo sa errorima ovdje da nes bolje vraca
		if (err || !user) {
			res.sendStatus(401);
		}

		//potreban je neki crypto jwt token
		var tokenValue = user._id;
		var userId = user._id;

		var token = new AccessToken({
			token: tokenValue,
			userId: userId
		});

		token.save(function(err, token) {
			res.json({
				'accessToken': tokenValue
			});
		});
	})(req, res);
};

exports.logout = function(req, res) {
	AccessToken.remove({
		userId: req.body.userId
	}, function(err) {
		if (err) {
			res.sendStatus(500);
		} else {
			res.sendStatus(200);
		}
	});
};

exports.loggedin = function(req, res) {

	//ovdje spremi usera u nesto sto cu moc koristit preko servera
	passport.authenticate('bearer', {
		session: false
	}, function(err, user, info) {

		//popravi ovo sa errorima ovdje da nes bolje vraca
		if (user) {
			res.json(_.pick(user, '_id', 'username'));
		} else {
			console.log(info);
			res.sendStatus(401);
		}
	})(req, res);
};

exports.signup = function(req, res) {

	var user = new User({
		username: req.body.username,
		password: req.body.password
	});

	user.save(function(err, user) {
		if (err) {
			res.sendStatus(500);
		} else {

			//ovo je CP onog iz logina, to treba promijeniti

			passport.authenticate('local', {
				session: false
			}, function(err, user) {

				//popravi ovo sa errorima ovdje da nes bolje vraca
				if (err || !user) {
					res.sendStatus(401);
				}

				//potreban je neki crypto jwt token
				var tokenValue = user._id;
				var userId = user._id;

				var token = new AccessToken({
					token: tokenValue,
					userId: userId
				});

				token.save(function(err, token) {
					res.json({
						'accessToken': tokenValue
					});
				});
			})(req, res);

		}
	});
};

exports.checkusername = function(req, res) {
	User.findOne({
		username: req.body.username
	}, function(err, users) {
		if (users !== null) {
			res.sendStatus(405);
		} else {
			res.sendStatus(200);
		}
	});
};