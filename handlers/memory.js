'use strict';

var Memory = require('./../models/memory.js');
var City = require('./../models/city.js');

exports.addMemory = function(req, res) {
	var memory = new Memory({
		userId: req.body.userId,
		cityId: req.body.cityId,
		startDate: req.body.startDate,
		endDate: req.body.endDate,
		review: req.body.review,
		rating: req.body.rating
	});

	memory.save(function(err, memory) {
		if (err) {
			res.sendStatus(500);
		} else {
			updateCityVisits(memory.cityId, true);
			res.sendStatus(200);
		}
	});
};

exports.getMemory = function(req, res) {

};

exports.updateMemory = function(req, res) {

};

exports.deleteMemory = function(req, res) {

};

exports.getMemories = function(req, res, next) {
	
	if(req.params.user) {
		next();
	}

	Memory.find({

	}, function(err, memories) {
		if(err) {
			res.sendStatus(500);
		} else {
			res.json(memories);
		}
	});
};

exports.getUserMemories = function(req, res) { 
	Memory.find({
		userId: req.params.user
	}, function(err, memories) {
		if (err) {
			res.sendStatus(500);
		} else {
			res.json(memories);
		}
	});
};

var updateCityVisits = function (cityId, visited) {
	City.findById(cityId, function(err, city) {
		var addVisit = (visited === true) ? 1 : -1;
		city.numberOfVisits += addVisit;

		city.save(function(err) {

		});
	});
};