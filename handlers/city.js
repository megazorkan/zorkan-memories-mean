'use strict';

var City = require('./../models/city.js');


exports.getCity = function(req, res) {
	var name = req.params.city;
	City.find({
		name: name
	}, function(err, city) {
		if (err) {
			res.sendStatus(500);
		}
		res.json(city);
	});
};

exports.addCity = function(req, res, next) {
	if (req.body.city) {
		next();
	} else {
		var name = req.body.name;
		var latitude = req.body.latitude;
		var longitude = req.body.longitude;
		var countryId = req.body.countryId;

		var city = new City({
			name: name,
			latitude: latitude,
			longitude: longitude,
			countryId: countryId
		});

		city.save(function(err, city) {
			if (err) {
				res.sendStatus(500);
			} else {
				res.json(city);
			}
		});
	}
};

exports.updateCity = function(req, res) {
	var id = req.body.id;
	var name = req.body.name;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	var countryId = req.body.countryId;

	City.findById(id, function(err, city) {
		if (err) {
			res.send(500);
		}
		city.name = name;
		city.latitude = latitude;
		city.longitude = longitude;
		city.countryId = countryId;

		city.save(function(err) {
			if (err) {
				res.send(500);
			}
			res.json(city);
		});
	});
};

exports.deleteCity = function(req, res) {
	var cityId = req.params.city;
	City.findByIdAndRemove(cityId, function(err) {
		if (err) {
			res.sendStatus(500);
		} else {
			res.sendStatus(200);
		}
	});
};

exports.getCities = function(req, res, next) {
	if (req.params.country !== undefined) {
		next();
	} else {
		City.find({}, function(err, cities) {
			if (err) {
				res.sendStatus(500);
			}
			res.json(cities);
		});
	}
};

exports.getCitiesByCountry = function(req, res) {
	var countryId = req.params.country;
	City.find({
		countryId: countryId
	}, function(err, cities) {
		if (err) {
			res.sendStatus(500);
		}
		res.json(cities);
	});
};

exports.deleteCities = function(req, res) {
	City.remove({}, function(err) {
		if (err) {
			res.sendStatus(500);
		} else {
			res.sendStatus(200);
		}
	});
};
