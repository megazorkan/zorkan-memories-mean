'use strict';

var Memory = require('./../models/memory.js');

var _ = require('underscore');

exports.getNewsfeed = function(req, res) {
	Memory.find({}, function(err, memories) {

		var filterMemories = _.filter(memories, function(memory) {
			return memory.userId != req.body.userId;
		});

		res.json(filterMemories);
	});
};