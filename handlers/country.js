'use strict';

var Country = require('./../models/country.js');

exports.getCountry = function(req, res) {
	var name = req.params.country;

	Country.find({
		name: name
	}, function(err, country) {
		if (err) {
			res.sendStatus(500);
		}
		res.json(country);
	});
};

exports.addCountry = function(req, res, next) {
	if (req.params.country) {
		next();
	} else {
		var country = new Country({
			name: req.body.name,
			latitude: req.body.latitude,
			longitude: req.body.longitude
		});

		//ovdje vrati ime countrya
		country.save(function(err, country) {
			if (err) {
				res.sendStatus(500);
			} else {
				res.json(country);
			}
		});
	}
};

exports.updateCountry = function(req, res) {
	var id = req.body.id;
	var name = req.body.name;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;

	Country.findById(id, function(err, country) {
		if (err) {
			res.send(500);
		}
		country.name = name;
		country.latitude = latitude;
		country.longitude = longitude;

		country.save(function(err) {
			if (err) {
				res.send(500);
			}
			res.json(country);
		});
	});
};

exports.deleteCountry = function(req, res) {
	var countryId = req.params.country;

	Country.findByIdAndRemove(countryId, function(err) {
		if (err) {
			res.sendStatus(500);
		} else {
			res.sendStatus(200);
		}
	});
};


exports.getCountries = function(req, res) {
	Country.find({}, function(err, countries) {
		if (err) {
			res.send(err);
		} else {
			res.json(countries);
		}
	});
};

exports.deleteCountries = function(req, res) {
	Country.remove({}, function(err) {
		if (err) {
			res.sendStatus(500);
		} else {
			res.sendStatus(200);
		}
	});
};