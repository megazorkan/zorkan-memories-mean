'use strict';

var authenticationHandler = require('./handlers/authentication.js');
var memoryHandler = require('./handlers/memory.js');
var countryHandler = require('./handlers/country.js');
var cityHandler = require('./handlers/city.js');
var userHandler = require('./handlers/user.js');
var newsfeedHandler = require('./handlers/newsfeed.js');

module.exports = function(app) {

	//authentication
	app.post('/api/login', authenticationHandler.login);
	app.post('/api/logout', authenticationHandler.logout);
	app.get('/api/loggedin', authenticationHandler.loggedin);
	app.post('/api/signup', authenticationHandler.signup);
	app.post('/api/checkusername', authenticationHandler.checkusername);

	//country
	app.route('/api/country/:country?')
		.get(countryHandler.getCountry)
		.post(countryHandler.addCountry)
		.put(countryHandler.updateCountry)
		.delete(countryHandler.deleteCountry);

	app.route('/api/countries')
		.get(countryHandler.getCountries)
		.delete(countryHandler.deleteCountries);

	//city
	app.route('/api/city/:city?')
		.get(cityHandler.getCity)
		.post(cityHandler.addCity)
		.put(cityHandler.updateCity)
		.delete(cityHandler.deleteCity);

	app.route('/api/cities/:country?')
		.get(cityHandler.getCities)
		.get(cityHandler.getCitiesByCountry)
		.delete(cityHandler.deleteCities);


	//memory	
	app.route('/api/memory/:memory?')
		.get(memoryHandler.getMemory)
		.post(memoryHandler.addMemory)
		.delete(memoryHandler.deleteMemory);

	app.route('/api/memories/:user?')		
		.get(memoryHandler.getMemories)
		.get(memoryHandler.getUserMemories);


	//newsfeed
	app.post('/api/newsfeed', newsfeedHandler.getNewsfeed);

	//user
	app.get('/api/users', userHandler.getUsers);


};