'use strict';

var passport = require('passport');
var User = require('./../models/user.js');
var localStrategy = require('./../strategies/auth/local.js');
var bearerStrategy = require('./../strategies/auth/bearer.js');

module.exports = function(app) {

	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	passport.use('local', localStrategy);

	passport.use('bearer', bearerStrategy);

	app.use(passport.initialize());

	app.use(passport.session());
}