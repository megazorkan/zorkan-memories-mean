'use strict';


angular.module('welcome', []);
angular.module('services', []);
angular.module('filters', []);
angular.module('dashboard', ['services']);
angular.module('newsfeed', ['services', 'filters']);
angular.module('utilities', ['services']);
angular.module('memory', ['services']);
angular.module('users', ['services']);
angular.module('user', ['services']);
angular.module('city', ['services']);
angular.module('cities', ['services']);
angular.module('country', ['services']);
angular.module('countries', ['services']);
angular.module('profile', ['services']);

angular.module('admin', ['services']);

angular.module('zorkanMemories', ['ui.router', 'LocalStorageModule', 'ngFileUpload',
	'welcome', 'dashboard', 'utilities', 'newsfeed', 'memory', 'user', 'users', 'city',
	'cities', 'country', 'countries', 'profile', 'admin', 'services', 'filters'
]);