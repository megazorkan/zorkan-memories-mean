'use strict';

angular.module('services')
	.constant('memoryUrl', '/api/memory')
	.constant('memoriesUrl', '/api/memories')
	.factory('memoryService', ['$http', 'userService', 'memoryUrl', 'memoriesUrl',
		function($http, userService, memoryUrl, memoriesUrl) {

			var addMemory = function(city, startDate, endDate, review, rating) {
				return $http.post(memoryUrl, {
					userId: userService.getId(),
					cityId: city._id,
					startDate: startDate,
					endDate: endDate,
					review: review,
					rating: rating
				});
			};

			var getUserMemories = function(userId) {
				return $http.get(memoriesUrl + '/' + userId);
			};


			var getMemories = function() {
				return $http.get(memoriesUrl);
			};

			return {
				addMemory: addMemory,
				getUserMemories: getUserMemories,
				getMemories: getMemories
			};
		}
	]);