'use strict';

angular.module('services')
	.factory('userService', function() {

		var user = {
			id: null,
			username: null,
			accessToken: null
		};

		var setId = function(id) {
			user.id = id;
		};

		var getId = function() {
			return user.id;
		};

		var setUsername = function(username) {
			user.username = username;
		};

		var getUsername = function() {
			return user.username;
		};

		var setAccessToken = function(accessToken) {
			user.accessToken = accessToken;
		};

		var getAccessToken = function() {
			return user.accessToken;
		};


		return {

			setId: setId,

			getId: getId,

			setUsername: setUsername,

			getUsername: getUsername,

			setAccessToken: setAccessToken,

			getAccessToken: getAccessToken

		};

	});