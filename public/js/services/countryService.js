'use strict';

angular.module('services')
	.constant('countryUrl', '/api/country')
	.constant('countriesUrl', '/api/countries')
	.factory('countryService', ['$http', 'countryUrl', 'countriesUrl',
		function($http, countryUrl, countriesUrl) {

			var getCountry = function(name) {
				return $http.get(countryUrl + '/' + name);
			};

			var addCountry = function(name, latitude, longitude) {
				return $http.post(countryUrl, {
					name: name,
					latitude: latitude,
					longitude: longitude
				});
			};

			var updateCountry = function(id, name, latitude, longitude) {
				return $http.put(countryUrl, {
					id: id,
					name: name,
					latitude: latitude,
					longitude: longitude
				});
			};

			var deleteCountry = function(countryId) {
				return $http.delete(countryUrl + '/' + countryId);
			};

			var getCountries = function() {
				return $http.get(countriesUrl);
			};

			var deleteCountries = function() {
				return $http.delete(countriesUrl);
			};

			var findCountry = function(name) {
				return $http.post(findCountryUrl, {
					name: name
				});
			};

			return {
				getCountry: getCountry,
				addCountry: addCountry,
				updateCountry: updateCountry,
				deleteCountry: deleteCountry,
				getCountries: getCountries,
				deleteCountries: deleteCountries,
				findCountry: findCountry
			};
		}
	]);