'use strict';

angular.module('services')
	.constant('userUrl', '/api/user')
	.constant('loginUrl', '/api/login')
	.constant('signupUrl', '/api/signup')
	.constant('logoutUrl', '/api/logout')
	.constant('loggedInUrl', '/api/loggedin')
	.constant('signupUrl', '/api/signup')
	.constant('checkUsernameUrl', '/api/checkusername')
	.factory('authenticationService', ['$http', '$q', '$state', 'localStorageService', 'userService', 'userUrl', 'loginUrl', 'logoutUrl', 'loggedInUrl', 'signupUrl', 'checkUsernameUrl',
		function($http, $q, $state, localStorageService, userService, userUrl, loginUrl, logoutUrl, loggedInUrl, signupUrl, checkUsernameUrl) {

			var login = function(user, pass) {
				return $http.post(loginUrl, {
					username: user,
					password: pass
				});
			};

			var signup = function(user, pass) {
				return $http.post(signupUrl, {
					username: user,
					password: pass,
				});
			};

			var logout = function() {
				return $http.post(logoutUrl, {
					userId: userService.getId()
				});
			};

			var loggedIn = function() {
				var deferred = $q.defer();
				$http.get(loggedInUrl, {
						headers: {
							'Authorization': 'Bearer ' + localStorageService.get('accessToken')
						}
					})
					.success(function(data) {
						userService.setId(data._id);
						userService.setUsername(data.username);
						deferred.resolve(data);
					})
					.error(function(error) {
						$state.go('login');
					});
			};


			var checkUsername = function(username) {
				return $http.post(checkUsernameUrl, {
					username: username
				});
			};

			return {
				login: login,

				signup: signup,

				logout: logout,

				loggedIn: loggedIn,

				checkUsername: checkUsername
			};

		}
	]);