'use strict';

angular.module('services')
	.constant('getUsersUrl', '/api/users')
	.factory('usersService', ['$http', 'getUsersUrl', function($http, getUsersUrl) {

		var getUsers = function() {
			return $http.get(getUsersUrl);
		};

		return {
			getUsers: getUsers
		};

	}]);