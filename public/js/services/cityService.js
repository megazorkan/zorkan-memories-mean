'use strict';

angular.module('services')
	.constant('cityUrl', '/api/city')
	.constant('citiesUrl', '/api/cities')
	.factory('cityService', ['$http', 'cityUrl', 'citiesUrl', function($http, cityUrl, citiesUrl) {

		var getCity = function(name) {
			return $http.get(cityUrl + '/' + name);
		};

		var addCity = function(name, latitude, longitude, countryId) {
			return $http.post(cityUrl, {
				name: name,
				latitude: latitude,
				longitude: longitude,
				countryId: countryId
			});
		};

		var updateCity = function(id, name, latitude, longitude, countryId) {
			return $http.put(cityUrl, {
				id: id,
				name: name,
				latitude: latitude,
				longitude: longitude,
				countryId: countryId
			});
		};

		var deleteCity = function(cityId) {
			return $http.delete(cityUrl + '/' + cityId);
		};

		var getCities = function() {
			return $http.get(citiesUrl);
		};

		var getCitiesByCountry = function(countryId) {
			return $http.get(citiesUrl + '/' + countryId);
		};

		var deleteCities = function() {
			return $http.delete(citiesUrl);
		};

		return {
			getCity: getCity,
			addCity: addCity,
			updateCity: updateCity,
			deleteCity: deleteCity,
			getCities: getCities,
			getCitiesByCountry: getCitiesByCountry,
			deleteCities: deleteCities
		};

	}]);