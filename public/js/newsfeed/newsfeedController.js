'use strict';

angular.module('newsfeed')
	.controller('newsfeedCtrl', ['$scope', 'memoryService', 'userService', 'usersService', 'cityService',
		function($scope, memoryService, userService, usersService, cityService) {

			$scope.getCities = function() {
				cityService.getCities()
					.success(function(data) {
						$scope.cities = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getUsers = function() {
				usersService.getUsers()
					.success(function(data) {
						$scope.users = data;
					}).error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getNewsfeed = function() {
				memoryService.getMemories()
					.success(function(data) {
						$scope.memories = data;
					}).error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getUsers();
			$scope.getCities();
			$scope.getNewsfeed();
		}
	]);