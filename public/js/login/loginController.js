'use strict';

angular.module('utilities')
	.controller('loginCtrl', ['$scope', '$state', 'localStorageService', 'userService', 'authenticationService',
		function($scope, $state, localStorageService, userService, authenticationService) {

			$scope.login = function(username, password) {
				authenticationService.login(username, password).success(function(data) {
					localStorageService.set('accessToken', data.accessToken);
					$state.go('user.dashboard');
				}).error(function(error) {
					$scope.loginError = error;
				});
			};

		}
	]);