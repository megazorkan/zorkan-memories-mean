;'use strict';

angular.module('memory')
	.controller('addmemoryCtrl', ['$scope', 'memoryService', 'cityService', 'countryService',
		function($scope, memoryService, cityService, countryService) {

			$scope.addMemory = function(cityId, startDate, endDate, review, rating) {
				memoryService.addMemory(cityId, startDate, endDate, review, rating)
					.success(function(data) {
						$scope.status = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getCities = function() {
				cityService.getCities()
					.success(function(data) {
						$scope.cities = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getCountries = function() {
				countryService.getCountries()
					.success(function(data) {
						$scope.countries = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.filterCities = function(country) {
				var countryId = country._id;
				$scope.filteredCities = _.filter($scope.cities, function(city) {
					return city.countryId === countryId;
				});
			};


			$scope.getCountries();
			$scope.getCities();

		}
	]);