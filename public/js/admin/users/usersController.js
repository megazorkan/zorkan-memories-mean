'use strict';

angular.module('admin')
	.controller('adminUsersCtrl', ['$scope', 'usersService', function($scope, usersService) {

		$scope.getUsers = function() {
			usersService.getUsers()
				.success(function(data) {
					$scope.users = data;
				})
				.error(function(error) {
					$scope.status = error;
				});
		};

		$scope.getUsers();
	}]);