'use strict';

angular.module('admin')
	.controller('adminCountriesCtrl', ['$scope', 'countryService',
		function($scope, countryService) {

			$scope.getCountry = function(name) {
				countryService.getCountry(name)
					.success(function(data) {
						$scope.selectedCountry = data[0];
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.addCountry = function(name, latitude, longitude) {
				countryService.addCountry(name, latitude, longitude)
					.success(function(data) {
						//vrati ime drzave
						$scope.status = 'Successfully added ' + data.name;
						$scope.getCountries();
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.updateCountry = function(id, name, latitude, longitude) {
				countryService.updateCountry(id, name, latitude, longitude)
					.success(function(data) {
						$scope.status = 'Update successfull!';
						$scope.getCountries();
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.deleteCountry = function(countryId) {
				countryService.deleteCountry(countryId)
					.success(function(data) {
						//vrati ime drzave
						$scope.status = 'Country deleted';
						$scope.removeSearchResult();
						$scope.getCountries();
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getCountries = function() {
				countryService.getCountries()
					.success(function(data) {
						$scope.countries = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.deleteCountries = function() {
				countryService.deleteCountries()
					.success(function(data) {
						$scope.status = 'All countries deleted';
						$scope.getCountries();
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.removeSearchResult = function() {
				$scope.selectedCountry = null;
			};

			//initialization
			$scope.getCountries();

		}
	]);