'use strict';

angular.module('admin')
	.controller('adminCitiesCtrl', ['$scope', 'cityService', 'countryService', '$http',
		function($scope, cityService, countryService, $http) {

			$scope.getCity = function(name) {
				cityService.getCity(name)
					.success(function(data) {
						$scope.selectedCity = data[0];
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.addCity = function(name, latitude, longitude, countryId) {
				cityService.addCity(name, latitude, longitude, countryId)
					.success(function(data) {
						//dodaj ime grada kojeg si dodao kime koje si dobio kroz response
						$scope.status = 'Successfully added city';
						$scope.getCities();
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.updateCity = function(id, name, latitude, longitude, countryId) {
				cityService.updateCity(id, name, latitude, longitude, countryId)
					.success(function(data) {
						$scope.status = 'Successfully updated city';
					})
					.error(function(error) {
						console.log(error);
						$scope.status = error;
					});
			};

			$scope.deleteCity = function(cityId) {
				cityService.deleteCity(cityId)
					.success(function(data) {
						$scope.status = data;
						$scope.removeSearchResult();
						$scope.getCities();
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getCities = function() {
				cityService.getCities()
					.success(function(data) {
						$scope.cities = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getCountries = function() {
				countryService.getCountries()
					.success(function(data) {
						$scope.countries = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.deleteCities = function() {
				cityService.deleteCities()
					.success(function(data) {

					})
					.error(function(error) {

					});
			};

			$scope.removeSearchResult = function() {
				$scope.selectedCity = null;
			};

			$scope.selectCity = function(name) {
				console.log(name);
			};

			//za testiranje fjumetovog get-a
			$scope.test = function() {
				console.log('zovem test');
				$http.get('bpsql.byethost6.com/api.bpsql/public/questions/1')
					.success(function(data) {
						console.log(data);
					})
					.error(function(error) {
						console.log(error);
					});
			};

			$scope.getCountries();
			$scope.getCities();
			//$scope.test();
		}
	]);