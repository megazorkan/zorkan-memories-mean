'use strict';

angular.module('filters')
	.filter('getUsernameById', function() {
		return function(userId, users) {
			var filteredUser = _.where(users, {
				_id: userId
			});

			var filteredUsername = _.pluck(filteredUser, 'username');
			return filteredUsername.toString();
		};
	})
	.filter('memberSince', function() {
		return function(user) {
			var memberSince = new Date(user.memberSince).getTime() / 1000;
			var thisMoment = new Date().getTime() / 1000;
			var passedDays = Math.floor((thisMoment - memberSince) / 86400);
			return passedDays;
		};
	});