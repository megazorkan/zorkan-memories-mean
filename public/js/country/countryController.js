'use strict';

angular.module('country')
	.controller('countryCtrl', ['$scope', '$stateParams', 'countryService', 'cityService',
		function($scope, $stateParams, countryService, cityService) {
			$scope.countryName = $stateParams.name;

			$scope.getCities = function(countryName) {
				countryService.getCountry(countryName)
					.then(function(data) {
							var countryId = data.data[0]._id;
							cityService.getCitiesByCountry(countryId)
								.success(function(data) {
									$scope.cities = data;
								})
								.error(function(error) {
									$scope.status = error;
								});
						},
						function(error) {
							$scope.status = error;
						});
			};

			$scope.getCities($scope.countryName);
		
	}]);