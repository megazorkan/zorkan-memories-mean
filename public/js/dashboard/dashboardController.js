'use strict';

angular.module('dashboard')
	.controller('dashboardCtrl', ['$scope', '$state', 'userService', 'authenticationService', function($scope, $state, userService, authenticationService) {

		$scope.username = userService.getUsername();
		$scope.logout = function() {
			authenticationService.logout().success(function(data) {
				$state.go('/');
			}).error(function(error) {
				$scope.status = error;
			});
		};
	}]);