'use strict';

angular.module('utilities')
	.controller('signupCtrl', ['$scope', '$location', 'authenticationService', 'userService',
		function($scope, $location, authenticationService, userService) {

			$scope.signup = function(username, password, repeatPassword) {

				if (password !== repeatPassword) {
					$scope.signupStatus = 'Passwords do not match';
				} else {
					authenticationService.checkUsername(username)
						.success(function(data) {
							authenticationService.signup(username, password)
								.success(function(data) {
									authenticationService.login(username, password);
								})
								.error(function(data) {
									$scope.signupStatus = 'Something went wrong. Try again later';
								});
						})
						.error(function(error) {
							$scope.signupStatus = 'Username already taken';
						});
				}
			};
		}
	]);