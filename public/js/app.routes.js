'use strict';

angular.module('zorkanMemories')
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

		$stateProvider

			//admin
			.state('admin', {
				url: '/admin',
				templateUrl: 'js/layouts/adminLayout.html',
				abstract: true
			})
			.state('admin.dashboard', {
				url: '/dashboard',
				templateUrl: 'js/admin/dashboard/dashboardView.html',
				controller: 'adminDashboardCtrl'
			})
			.state('admin.cities', {
				url: '/cities',
				templateUrl: 'js/admin/cities/citiesView.html',
				controller: 'adminCitiesCtrl'
			})
			.state('admin.countries', {
				url: '/countries',
				templateUrl: 'js/admin/countries/countriesView.html',
				controller: 'adminCountriesCtrl'
			})
			.state('admin.users', {
				url: '/users',
				templateUrl: 'js/admin/users/usersView.html',
				controller: 'adminUsersCtrl'
			})

			//user
			.state('user', {
				url: '/user',
				templateUrl: 'js/layouts/userLayout.html',
				abstract: true
			
			})
			.state('user.dashboard', {
				url: '/dashboard',
				templateUrl: 'js/dashboard/dashboardView.html',
				controller: 'dashboardCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}
			})
			.state('user.add_memory', {
				url: '/add_memory',
				templateUrl: '/js/addmemory/addmemoryView.html',
				controller: 'addmemoryCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})
			.state('user.memories', {
				url: '/memories',
				templateUrl: '/js/memories/memoriesView.html',
				controller: 'memoriesCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})			
			.state('user.news_feed', {
				url: '/news_feed',
				templateUrl: '/js/newsfeed/newsfeedView.html',
				controller: 'newsfeedCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})
			.state('user.user', {
				url: '/user/:name',
				templateUrl: 'js/user/userView.html',
				controller: 'userCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})			
			.state('user.users', {
				url: '/users',
				templateUrl: 'js/users/usersView.html',
				controller: 'usersCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})
			.state('user.city', {
				url: '/city/:name',
				templateUrl: 'js/city/cityView.html',
				controller: 'cityCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})
			.state('user.cities', {
				url: '/cities',
				templateUrl: 'js/cities/citiesView.html',
				controller: 'citiesCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})

			.state('user.country', {
				url: '/country/:name',
				templateUrl: 'js/country/countryView.html',
				controller: 'countryCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})
			.state('user.countries', {
				url: '/countries',
				templateUrl: 'js/countries/countriesView.html',
				controller: 'countriesCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})
			.state('user.profile', {
				url: '/profile',
				templateUrl: 'js/profile/profileView.html',
				controller: 'profileCtrl',
				resolve: {
					auth: function(authenticationService) {
						return authenticationService.loggedIn();
					}
				}				
			})


			//miscellaneous
			.state('error', {
				url:'/error',
				templateUrl: 'js/layouts/errorLayout.html'
			})
			.state('welcome', {
				url: '/',
				templateUrl: 'js/welcome/welcomeView.html',
				controller: 'welcomeCtrl'
			})
			.state('login', {
				url: '/login',
				templateUrl: 'js/login/loginView.html',
				controller: 'loginCtrl'
			})
			.state('signup', {
				url: '/signup',
				templateUrl: 'js/signup/signupView.html',
				controller: 'signupCtrl'
			});
			
			$urlRouterProvider.otherwise('error');
	}]);