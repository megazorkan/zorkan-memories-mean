'use strict';

angular.module('memory')
	.controller('memoriesCtrl', ['$scope', 'memoryService', 'userService', 'cityService',
		function($scope, memoryService, userService, cityService) {
			
			$scope.getCities = function() {
				cityService.getCities()
					.success(function(data) {
						$scope.cities = data;
					})
					.error(function(error) {
						$scope.status = error;
					});
			};

			$scope.getUserMemories = function(userId) {
				memoryService.getUserMemories(userId).success(function(data) {
					$scope.memories = data;
				}).error(function(error) {
					$scope.error = error;
				});
			};

			$scope.getCities();
			$scope.getUserMemories(userService.getId());

		}
	]);