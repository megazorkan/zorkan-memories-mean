'use strict';

var BearerStrategy = require('passport-http-bearer').Strategy;
var User = require('../../models/user');
var AccessToken = require('./../../models/accessToken.js');

var bearerAuthStrategy = new BearerStrategy(function(accessToken, done) {

	AccessToken.findOne({
		token: accessToken
	}, function(err, token) {
		if (err) {
			return done(err);
		}
		if (!token) {
			return done(null, false);
		}

		//check to see if it expired

		User.findById(token.userId, function(err, user) {
			if (err) {
				return done(err);
			}
			if (!user) {
				return done(null, false, {
					message: 'Unknown user'
				});
			}

			done(null, user);
		});
	});
});

module.exports = bearerAuthStrategy;